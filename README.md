# minigrep

A toy grep-like program to demonstrate:

* Organizing code
* Using vectors and strings
* Handling errors
* Using traits and lifetimes where appropriate
* Writing tests

Also, some closures, iterators, and trait objects.
